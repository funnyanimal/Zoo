package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import model.Animal;
import model.AnimalType;
import model.Cat;
import model.CatType;
import model.Dog;

/**
 * Service for generation animals
 * 
 * @author Пользователь
 * 
 * @param <T>
 */
public class AnimalGeneratortForFun {
	
	public static List<Animal> generateAnimal(int numOfAnimal, String typeOfAnimal) {
		if(typeOfAnimal.equals(AnimalType.CAT.toString())) return generateCats(numOfAnimal);
		else return generateDogs(numOfAnimal);
	}
	
	/**
	 * Generate cats
	 * 
	 * @param numOfCats
	 * @return
	 */
	public static List<Animal> generateCats(int numOfCats) {
		List<Animal> cats = new ArrayList<>();
		
		for(int i = 0; i < numOfCats; i++) {
			Cat cat = new Cat();
			cat.setName("cat " + i);
			cat.setAge(i);
			cat.setType(CatType.KITTEN);
			cats.add(cat);
		}
		
		return cats;
	}
	
	/**
	 * Generate dogs
	 * 
	 * @param numOfDogs
	 * @return
	 */
	public static List<Animal> generateDogs(int numOfDogs) {
		List<Animal> dogs = new ArrayList<>();
		
		for(int i = 0; i < numOfDogs; i++) {
			Dog dog = new Dog();
			dog.setName("cat " + i);
			dog.setAge(i);
			dog.setBigTeeth(i % 2 == 0 ? true : false);
			dogs.add(dog);
		}
		
		return dogs;
	}

}
