package com.example.demo.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import model.Animal;

/**
 * Service implementation
 * 
 * @author Пользователь
 */
@Service
public class AnimalServiceImpl implements AnimalService {

	@Override
	public void create(Animal animal) {
		// TODO after connection with DB

	}

	@Override
	public Animal findAnimalByName(List<Animal> animals, String name) {
		return animals.stream().filter((s)->s.getName().startsWith(name)).findAny().orElse(null);	
	}

	@Override
	public Map<String, Animal> convertToMap(List<Animal> animals) {
		return animals.stream().collect(Collectors.toMap(Animal::getName, animal -> animal));
	}

}
