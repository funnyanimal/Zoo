package com.example.demo.service;

import model.Animal;

/**
 * Just for fun and future improvement
 * @author Пользователь
 */
public class NoiseService <T extends Animal> {
	private T objectMakingNoise;

	public NoiseService(T objectMakingNoise) {
		this.objectMakingNoise = objectMakingNoise;
	}
	
	/**
	 * Provide a noise of any animals in the world
	 * 
	 * @return
	 */
	public String makeNoise() {
		return objectMakingNoise.makeNoise();
	}
}
