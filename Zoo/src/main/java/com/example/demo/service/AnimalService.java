package com.example.demo.service;

import java.util.List;
import java.util.Map;

import model.Animal;

/**
 * Represents operation for studying StreamApi functionality
 * 
 * @author Пользователь
 */
public interface AnimalService {
	   
	/**
	 * Create new animal
	 * 
	 * Create animal
	 * @param animal
	 */
    void create(Animal animal);
    
  /**
   * Find by animal by name
   * 
   * @param animals
   * @param name
   * @return
   */
    Animal findAnimalByName(List<Animal> animals, String name);
    
  /**
   * Convert to map
   * 
   * @param animal
   * @return
   */
    Map<String, Animal> convertToMap(List<Animal> animal);

}
