package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.service.AnimalGeneratortUtil;
import com.example.demo.service.AnimalService;

import model.Animal;

/**
 * in progress
 * 
 * @author Пользователь
 */
@RestController
public class AnimalController {
	private final AnimalService animalService;

    @Autowired
    public AnimalController(AnimalService animalService) {
        this.animalService = animalService;
	}
	
	@GetMapping(value = "/animals/{name}")
	public ResponseEntity<Animal> findByName(@PathVariable(name = "name") String name) {
		
	   List<Animal> animals = AnimalGeneratortUtil.generateCats(3);
	   Animal animal = animalService.findAnimalByName(animals, name);

	   return animal != null
	           ? new ResponseEntity<>(animal, HttpStatus.OK)
	           : new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
