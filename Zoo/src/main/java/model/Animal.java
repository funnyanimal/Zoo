package model;

import java.util.List;
import java.util.Objects;

/**
 * Model for Aimals 
 * 
 * @author Пользователь
 */
public abstract class Animal {
	private int age;
	private boolean isAngry;
	private String name;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isAngry() {
		return isAngry;
	}
	public void setAngry(boolean isAngry) {
		this.isAngry = isAngry;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return
	 */
	public abstract String makeNoise();
	
	@Override
	public int hashCode() {
		return Objects.hash(age, isAngry, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		return age == other.age && isAngry == other.isAngry && Objects.equals(name, other.name);
	}
	

	
}
