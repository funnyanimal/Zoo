package model;

public enum CatType {
	TIGER("orange", 12),
	KITTEN("grey", 3),
	LEW("white", 15);
		
	private final String color;
	private final int size;

	CatType(String color, int size) {
		this.size = size;
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public int getSize() {
		return size;
	}
}
