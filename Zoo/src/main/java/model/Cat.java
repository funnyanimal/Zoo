package model;

import java.util.Objects;

public class Cat extends Animal {
	private CatType type;
	private final String noise = "miayyyyyyy";

	public CatType getType() {
		return type;
	}

	public void setType(CatType type) {
		this.type = type;
	}
	
	@Override
	public String makeNoise() {
		return noise;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(type);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cat other = (Cat) obj;
		return type == other.type;
	}

	
}
