package model;

import java.util.Objects;

/**
 * Dog
 * 
 * @author Пользователь
 */
public class Dog extends Animal {
	private boolean isBigTeeth;
	private int numOfWalkingHour;
	private final String noise = "Gayyyyyy";
	
	public boolean isBigTeeth() {
		return isBigTeeth;
	}
	public void setBigTeeth(boolean isBigTeeth) {
		this.isBigTeeth = isBigTeeth;
	}
	public int getNumOfWalkingHour() {
		return numOfWalkingHour;
	}
	public void setNumOfWalkingHour(int numOfWalkingHour) {
		this.numOfWalkingHour = numOfWalkingHour;
	}
	
	@Override
	public String makeNoise() {
		return noise;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(isBigTeeth, numOfWalkingHour);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dog other = (Dog) obj;
		return isBigTeeth == other.isBigTeeth && numOfWalkingHour == other.numOfWalkingHour;
	}
	
}
